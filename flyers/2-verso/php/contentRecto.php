
<div class="contentcontent">


<img class="fond" src="img/tete_18.png" alt="" />

<div class=" txt txtleft">
	<h1><i></i></h1>
	
	<p><i>Impersonate</i> est une installation numérique qui aborde les interactions sociales des nouveaux médias et leur nature algorithmique
		et automatisée. L'aspect absurde du dispositif permet de mettre en avant ce que pourraient devenir les interactions automatisées, ses dérives,
		ses usages et ses avantages.  </p><br>

	<p>L'installation montre un échange entre deux chat-bots ou intelligences artificielles de discussion – programmes
		autonomes qui sont capables d'adapter leurs réponses à leurs interlocuteurs, d’ordinaire humains – qui, au lieu de se répondre
		par le texte, donnent chacun à tour de rôle leur réplique via un court extrait de vidéo YouTube.</p><br>

	<p>Pour obtenir cette conversation, notre installation lance automatiquement une recherche à partir de chaque réplique textuelle
		des chat-bots dans les sous-titres YouTube. L'extrait ayant les sous-titres les plus proches de ce qu'écrit le bot sera affiché.</p><br>

	<p>Dans l'installation, les programmes prennent une apparence humaine ou  l'apparence de tout autre personnage apparaissant
		sur la vidéo&#8239;:  </p><br>



</div>

<div class=" txt txtright">

	<p>ils s'anthropomorphisent. Les protagonistes filmés deviennent les marionnettes des programmes informatiques,
	d'où le titre <i>Impersonate</i>, qui signifie&#8239;: prendre le caractère ou l'apparence de quelqu'un, souvent de manière frauduleuse.  </p><br>
	<p>L'installation utilise deux espaces&#8239;: d'un côté, le matériel (vidéo projecteur, ordinateur, enceinte, câble, etc.)
		et en face, les informations audiovisuelles (projection vidéo et ondes sonores). Le matériel de l'installation est rendu visible.
		Cela permet de montrer le <em>hardware</em>, où les informations et les calculs se produisent concrètement alors que de l'autre côté
		il n'y a que des projections&#8239;: les images des visages, des gestes, des expressions, des intonations et des accents.</p><br>
	<p class="sous-info"><i>IVRO</i>&#8239;: <span>Romain Marula et Ivan Murit (Bruxelles&#8239;/&#8239;Paris) est un projet de recherche artistique
		qui aborde les interactions sociales des nouveaux médias et leur nature algorithmique et automatisée. </span>
	<br>www.ivro.fr</p>

<p class="colophon">
	<br>Cet objet a été mis en forme en Html et Css </br> par <b>Romain Marula</b> &amp; <b>Étienne Ozeray</b>, imprimé en riso chez <b>Ronan Deriez</b> sur du papier <b>Arcoprint 120 gr</b>. Les caractères
       typographiques utilisés sont <b>Meyrin</b> et <b>Open sans</b>, tout deux sous licence libre.<br>
        
      </p>
</div>




</div>
