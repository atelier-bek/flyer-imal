<?php
  include('inc/variables.php');
  include('inc/head.php');
?>

<div id="pageRecto" class="page">
  <div class="grids">
    <div class="fold"></div>
    <div class="grid gridRow">
      <?php for ($i=0; $i <= 33; $i++) { ?>
        <div class="row row<?= $i ?>"></div>
      <?php } ?>
    </div>
    <div class="grid gridCol">
      <?php for ($i=0; $i <= 33; $i++) { ?>
        <div class="col col<?= $i ?>"></div>
      <?php } ?>
    </div>
  </div>
</div>
<div id="contentRecto" class="content">
  <?php include('php/contentVerso.php'); ?>
</div>
<!-- <div id="pageVerso" class="page">
  <div class="grids">

  </div>
</div>
<div id="contentVerso" class="content">
  <?php include('php/contentVerso.php'); ?>
</div> -->

<?php include('inc/foot.php'); ?>
