

<div class="contentcontent">

<div class="info">
<p class="nom"><sup>Impersonate</sup></br>
Romain Marula &amp; Ivan Murit</p>

<p class="date"><span><em>Du 9 juin 2017 au 30 juin 2017</em></span>
<span><em>Vernissage le 8 juin 2017</em></span>
<span><em>18:00 — 20:00</em></span></p>
<p class="lieu"><span><em>iMAL, Center for Digital Cultures and Technology</em></br>
<span><em>Quai des Charbonnages, 30</em></span></br>
<span><em>1080 Molenbeek-Saint-Jean</em></span></p>
</div>

<div class="titre"><i>Impersonate</i></div>

<p><i>Impersonate</i> est une installation numérique qui aborde les interactions sociales des nouveaux médias et leur nature algorithmique
	et automatisée. L'aspect absurde du dispositif permet de mettre en avant ce que pourraient devenir les interactions automatisées, ses dérives,
	ses usages et ses avantages.  </p>

<p>L'installation montre un échange entre deux chat-bots ou intelligences artificielles de discussion – programmes
	autonomes qui sont capables d'adapter leurs réponses à leurs interlocuteurs, d’ordinaire humains – qui, au lieu de se répondre
	par le texte, donnent chacun à tour de rôle leur réplique via un court extrait de vidéo YouTube.</p>

<p>Pour obtenir cette conversation, notre installation lance automatiquement une recherche à partir de chaque réplique textuelle
	des chat-bots dans les sous-titres YouTube. L'extrait ayant les sous-titres les plus proches de ce qu'écrit le bot sera affiché.</p>

<p>Dans l'installation, les programmes prennent une apparence humaine ou  l'apparence de tout autre personnage apparaissant
	sur la vidéo : ils s'anthropomorphisent. Les protagonistes filmés deviennent les marionnettes des programmes informatiques,
	d'où le titre <i>Impersonate</i>, qui signifie : prendre le caractère ou l'apparence de quelqu'un, souvent de manière frauduleuse.  </p>

<p>L'installation utilise deux espaces : d'un côté, le matériel (vidéo projecteur, ordinateur, enceinte, câble, etc.)
	et en face, les informations audiovisuelles (projection vidéo et ondes sonores). Le matériel de l'installation est rendu visible.
	Cela permet de montrer le "hardware", où les informations et les calculs se produisent concrètement alors que de l'autre côté
	il n'y a que des projections : les images des visages, des gestes, des expressions, des intonations et des accents.</p>
</div>

<p class="sous-info"><em>IVRO</em>:<span>Romain Marula et Ivan Murit (Bruxelles / Paris) est un projet de recherche artistique
	qui aborde les interactions sociales des nouveaux médias et leur nature algorithmique et automatisée. </span>

<b>ivro.fr</b></p>
<div class="marge margeHoriz margeTop"></div>
<div class="marge margeHoriz margeBottom"></div>
<div class="marge margeVert margeLeft"></div>
<div class="marge margeVert margeRight"></div>
