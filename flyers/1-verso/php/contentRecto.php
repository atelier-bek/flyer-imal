

<div class="contentcontent">

	<div class=" txt txtleft">
		<p>
			<i>Impersonate</i> est une installation numérique qui aborde les interactions sociales des nouveaux médias et leur nature algorithmique et automatisée. L'aspect absurde du dispositif permet de mettre en avant ce que pourraient devenir les interactions automatisées, ses dérives, ses usages et ses avantages.
		</p>
		<br>
		<p>L'installation montre un échange entre deux chat-bots ou intelligences artificielles de discussion – programmes autonomes qui sont capables d'adapter leurs réponses à&nbsp;leurs interlocuteurs, d’ordinaire humains – qui, au lieu de se répondre par le texte, donnent chacun à tour de&nbsp;rôle leur réplique via un court extrait de vidéo YouTube.
		</p>
		<br>
		<p>
			Pour obtenir cette conversation, notre installation lance automatiquement une recherche à partir de chaque réplique textuelle des chat-bots dans les sous-titres YouTube. L'extrait ayant les sous-titres les plus proches de ce qu'écrit le bot sera affiché.
		</p>
		<br>
		<p>
			Dans l'installation, les programmes prennent une apparence humaine ou  l'apparence de tout autre personnage apparaissant sur la vidéo&#8239;: ils s'anthro-pomorphisent. Les protagonistes
		</p>
	</div>
	<div class="txt txtright">
		<p>
			 filmés deviennent les&nbsp;marionnettes des programmes informatiques, d'où&nbsp;le titre <i>Impersonate</i>, qui signifie&#8239;: prendre le&nbsp;caractère ou l'apparence de&nbsp;quelqu'un, souvent de manière frauduleuse.
		</p>
		<br>
		<p>
			L'installation utilise deux espaces&#8239;: d'un côté, le matériel (ordinateur, vidéo-projecteur, enceinte, câble) et&nbsp;en face, les informations audiovisuelles (projection vidéo et&nbsp;ondes sonores). Le matériel de&nbsp;l'installation est rendu visible. Cela&nbsp;permet de montrer le <em>hardware</em>, où les&nbsp;informations et les calculs se&nbsp;produisent concrètement alors que&nbsp;de&nbsp;l'autre côté il n'y a que des projections&#8239;: les images des visages, des gestes, des expressions, des intonations et des accents.
		</p>
		<br>
		<p>
			<i class="not">IVRO</i>&#8239;: Romain Marula et Ivan Murit (Bruxelles&#8239;/&#8239;Paris) est un projet de recherche artistique qui aborde les interactions sociales des nouveaux médias et leur nature algorithmique et automatisée.<br>
			www.ivro.fr
		</p>
	</div>

	<p class="colophon">
		Design graphique: Romain Marula &amp; Étienne Ozeray, <br/>méthode: Html/Css, impression: Ronan Deriez
	</p>
	<img class="image imageD" src="img/tete_24.png" alt="" />
	<img class="image imageD" src="img/tete_22.png" alt="" />
	<img class="image imageG" src="img/tete_23.png" alt="" />
	  </div>
