Romain Marula et Ivan Murit sont diplômés d’un master de l’école Supérieure d’art et de design de Grenoble-Valence en France où ils ont commencé à développer leur collaboration. C’est donc dans la complicité qu’ils élaborent leur recherches plastiques par le croisement de point de vue complémentaires. Romain s’intéresse en premier lieu aux échanges sociaux dans le numérique et Ivan aux impacts des algorithmes en prenant appuis sur sa pratique de la programmation. Ils ont participé à quelques festivals ou expositions d’art numérique comme le Désert Numerique (St Nazaire), électroni[k] (Rennes), iMAL (Bruxelles), Pass (Mons), Transient (Paris) ou Global Proxy à la biennale NEMO (Paris) et réalisé une résidence à la Villa Médicis en tant que lauréat en 2016.

2017
Exposition Impersonate, à l’iMAL, Bruxelles.
Exposition / atelier au Pass, Mons 

2016
Exposition Autopost, au Transient, Paris. Lauréat en arts visuels à la Villa Médicis, Rome. Exposition Autopost, à l’iMAL, Bruxelles.

2015
Parution édition de recherche Autopost Soutenance DNSEP: présentation d'Autopost, Valence.
